#include <string>
#include <vector>
#include <iostream>

struct item
{
	item *previous;
	std::string str;
};

void powerset( char **inputVector, int left, item *upper, std::vector<std::vector<std::string>> &result)
{
	item item;

	if (!left)
	{
		std::vector<std::string> tmpResult;

		while (upper)
		{
			tmpResult.push_back(upper->str);
			upper	= upper->previous;
		}

		result.push_back(tmpResult);
	}
	else
	{
		item.previous	= upper;
		item.str		= *inputVector;

		powerset(inputVector + 1, left - 1, upper, result);
		powerset(inputVector + 1, left - 1, &item, result);
	}
}

int main(int argc, char **argv)
{
	std::vector<std::vector<std::string>> result;
	powerset(argv+1, argc-1, NULL, result);

	for each (std::vector<std::string> item in result)
	{
		std::cout << "[";

		int counter = 0;

		for each (std::string str in item)
		{
			std::cout << str << " ";

			counter++;
		}

		if (!counter)
		{
			std::cout << "]" << std::endl;
		}
		else
		{
			std::cout << "\b]" << std::endl;
		}
		
	}
}