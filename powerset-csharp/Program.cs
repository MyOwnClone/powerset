﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace powerset_csharp
{
    class Program
    {
        public static IEnumerable<IEnumerable<T>> GetPowerSetLINQ<T>(List<T> list)
        {
            return from m in Enumerable.Range(0, 1 << list.Count)
                   select
                       from i in Enumerable.Range(0, list.Count)
                       where (m & (1 << i)) != 0
                       select list[i];
        }

        public static IEnumerable<IEnumerable<T>> GetPowerSet<T>(List<T> list)
        {
            var result = new List<List<T>>();
            var length = 1 << list.Count;

            for (int i = 0; i < length; i++)
            {
                List<T> tmp = new List<T>();

                for (int j = 0; j < list.Count; j++)
                {
                    var cond = (i & (1 << j));

                    if (cond != 0 )
                    {
                        tmp.Add(list[j]);
                    }
                }

                result.Add(tmp);
            }

            return result;
        }

        public static IEnumerable<IEnumerable<T>> GetPowerSetLINQAlternative<T>(IEnumerable<T> input)
        {
            var seed = new List<IEnumerable<T>>() { Enumerable.Empty<T>() }
              as IEnumerable<IEnumerable<T>>;

            return input.Aggregate(seed, (a, b) =>
              a.Concat(a.Select(x => x.Concat(new List<T>() { b }))));
        }

        static void Main(string[] args)
        {
            var colors = new List<ConsoleColor> { ConsoleColor.Red, ConsoleColor.Green, ConsoleColor.Blue, ConsoleColor.Yellow };
            var result = GetPowerSetLINQAlternative(colors);

            Console.Write(string.Join(Environment.NewLine, result.Select(subset => string.Join(",", subset.Select(clr => clr.ToString()).ToArray())).ToArray()));
        }
    }
}
