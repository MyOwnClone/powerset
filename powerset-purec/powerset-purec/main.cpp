#include <stdio.h>

struct node {
	char *string;
	struct node* previous;
};

void powerset(char **vector, int n, struct node *up)
{
	struct node me;

	if (!n) {
		putchar('[');
		while (up) {
			printf(" %s", up->string);
			up = up->previous;
		}
		puts(" ]");
	}
	else {
		me.string = *vector;
		me.previous = up;
		powerset(vector + 1, n - 1, up);
		powerset(vector + 1, n - 1, &me);
	}
}

int main(int argc, char **argv)
{
	powerset(argv + 1, argc - 1, 0);
	return 0;
}